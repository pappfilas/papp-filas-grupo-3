package com.faculdadecotemig.CF01_CodigoFonte.model;

import java.util.Set;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;

@Entity
public class Loja {
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	
	private Integer id;
	private String nome;
	private String cnpj;
	private String email;
	private String senha;

	@OneToMany(mappedBy="loja")
	private Set<Produto> produtos;
	@OneToMany(mappedBy="loja")
	private Set<LojaEvento> lojaeventos;
	
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public String getNome() {
		return nome;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}
	public String getCnpj() {
		return cnpj;
	}
	public void setCnpj(String cnpj) {
		this.cnpj = cnpj;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getSenha() {
		return senha;
	}
	public void setSenha(String senha) {
		this.senha = senha;
	}
	public Set<Produto> getProdutos() {
		return produtos;
	}
	public void setProdutos(Set<Produto> produtos) {
		this.produtos = produtos;
	}
	public Set<LojaEvento> getLojaeventos() {
		return lojaeventos;
	}
	public void setLojaeventos(Set<LojaEvento> lojaeventos) {
		this.lojaeventos = lojaeventos;
	}	
}
