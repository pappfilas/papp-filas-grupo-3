package com.cotemig.CF01_pAppFilas.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.cotemig.CF01_pAppFilas.model.AgendaEvento;

@Repository("agendaRepository")
public interface AgendaRepository extends JpaRepository<AgendaEvento, Integer> {
	 // method
}
