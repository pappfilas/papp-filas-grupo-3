package com.cotemig.CF01_pAppFilas.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.cotemig.CF01_pAppFilas.model.Agenda;

@Repository("agendaeventoRepository")
public interface AgendaEventoRepository extends JpaRepository<Agenda, Integer> {
	 // method
}
