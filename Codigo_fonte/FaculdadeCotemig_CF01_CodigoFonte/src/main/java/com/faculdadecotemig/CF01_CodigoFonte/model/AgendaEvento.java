package com.faculdadecotemig.CF01_CodigoFonte.model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

@Entity
public class AgendaEvento {
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	
	private Integer id;
	
	@ManyToOne
    @JoinColumn(name="agenda_id", nullable=false)
    private Agenda agenda;
	@ManyToOne
    @JoinColumn(name="evento_id", nullable=false)
    private Evento evento;
	
	
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public Agenda getAgenda() {
		return agenda;
	}
	public void setAgenda(Agenda agenda) {
		this.agenda = agenda;
	}
	public Evento getEvento() {
		return evento;
	}
	public void setEvento(Evento evento) {
		this.evento = evento;
	}
	
	
}
