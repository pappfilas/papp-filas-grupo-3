package com.faculdadecotemig.CF01_CodigoFonte.model;

import java.util.Set;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;

@Entity
public class Agenda {
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	
	private Integer id;

	@OneToOne(mappedBy = "agenda")
    private Cliente cliente;
    @OneToMany(mappedBy="agenda")
	private Set<AgendaEvento> agendaeventos;

	
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public Cliente getCliente() {
		return cliente;
	}
	public void setCliente(Cliente cliente) {
		this.cliente = cliente;
	}
	public Set<AgendaEvento> getAgendaeventos() {
		return agendaeventos;
	}
	public void setAgendaeventos(Set<AgendaEvento> agendaeventos) {
		this.agendaeventos = agendaeventos;
	}
}
