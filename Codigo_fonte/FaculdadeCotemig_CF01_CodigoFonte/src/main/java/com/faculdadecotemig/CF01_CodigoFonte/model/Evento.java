package com.faculdadecotemig.CF01_CodigoFonte.model;

import java.util.Set;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;

@Entity
public class Evento {
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	
	private Integer id;
	private String nome;
	private String local;
	private String data;
	
	@ManyToOne
    @JoinColumn(name="produtor_id", nullable=false)
    private Produtor produtor;
    @OneToMany(mappedBy="evento")
	private Set<AgendaEvento> agendaeventos;
	@OneToMany(mappedBy="evento")
	private Set<LojaEvento> lojaeventos;
    
	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public String getLocal() {
		return local;
	}

	public void setLocal(String local) {
		this.local = local;
	}

	public String getData() {
		return data;
	}

	public void setData(String data) {
		this.data = data;
	}

	public Produtor getProdutor() {
		return produtor;
	}

	public void setProdutor(Produtor produtor) {
		this.produtor = produtor;
	}

	public Set<AgendaEvento> getAgendaeventos() {
		return agendaeventos;
	}
	public void setAgendaeventos(Set<AgendaEvento> agendaeventos) {
		this.agendaeventos = agendaeventos;
	}
	public Set<LojaEvento> getLojaeventos() {
		return lojaeventos;
	}
	public void setLojaeventos(Set<LojaEvento> lojaeventos) {
		this.lojaeventos = lojaeventos;
	}	
}
