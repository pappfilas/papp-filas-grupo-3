package com.cotemig.CF01_pAppFilas.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.cotemig.CF01_pAppFilas.model.Evento;


@Repository("eventoRepository")
public interface EventoRepository extends JpaRepository<Evento, Integer> {
	 // method
}
